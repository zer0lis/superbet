import {NextFunction, Request, Response, Router} from "express";
import {Service, UserService } from "../user/user.service";
import {DeleteResult} from "typeorm";
import {Controller} from "../shared/interface/controller.interface";
import {PermissionEntity} from "./entity/permission.entity";
import {authenticateMiddleware} from "../middlewares/authenticate.middleware";
import {authorizeMiddleware} from "../middlewares/authorize.middleware";
import { authorizeIsAdminMiddleware } from "../middlewares/authorize-is-admin.middleware";


export class PermissionController implements Controller {
    public path = '/permission';
    public router: Router = Router();
    permissionService: Service;

    constructor(PermissionService: Service) {
        this.permissionService = PermissionService;
        this.initializeRoutes();

    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.getAll);
        this.router.get(`${this.path}/:id`,  [authenticateMiddleware, authorizeIsAdminMiddleware], this.getById);       // all authenticated users
        this.router.post(`${this.path}`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.create);
        this.router.put(`${this.path}/:id`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.update);
        this.router.delete(`${this.path}/:id`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.delete);
    }

    getAll = async (req: any, res: Response, next: NextFunction) => {
        this.permissionService.getAll()
            .then((permissions: PermissionEntity[]) => res.json(permissions))
            .catch((err: Error) => next(err));
    }

    getById = async (req: any, res: Response, next: NextFunction) => {
        const id = req.params.id;

        this.permissionService.getById(id)
            .then((permission: PermissionEntity) => permission ? res.json(permission) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    create = async (req: any, res: Response, next: NextFunction) => {
        const newPermission = req.body.permission;

        if(!newPermission) return res.status(401).json({ message: 'No permission post data' });

        this.permissionService.create(newPermission)
            .then((permission: PermissionEntity) => permission ? res.json(permission) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    update = async (req: any, res: Response, next: NextFunction) => {
        const newPermission = req.body.permission;

        if(!newPermission) return res.status(401).json({ message: 'No user post data' });

        this.permissionService.update(newPermission)
            .then((permission: PermissionEntity) => permission ? res.json(permission) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    delete = async (req: any, res: Response, next: NextFunction) => {
        const id = req.params.id;

        this.permissionService.delete(id)
            .then((status: DeleteResult) => status ? res.json({message: 'UserEntity deleted'}) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

}

