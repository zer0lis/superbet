import {DepartmentEntity} from "../../../department/entity/department.entity";
import {PermissionEntity} from "../../../permission/entity/permission.entity";
import {Status} from "../../../shared/interface/status.type";

export interface CreateUserDto {
    username: string;
    password: string;
    first_name?: string;
    last_name?: string;
    email?: string;
    phone_number?: string; // because javascript numbers starting with 0 are ignored
    departments?: DepartmentEntity[] | null;
    permissions?: PermissionEntity[];
    status?: Status;
}