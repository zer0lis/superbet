import {Service} from "../user/user.service";
import {DeleteResult, getRepository} from "typeorm";
import {PermissionEntity} from "./entity/permission.entity";
import {UpdatePermissionDto} from "./interface/update-permission.dto";
import {CreatePermissionDto} from "./interface/create-permission.dto";


export class PermissionService implements Service {

    getAll = async (): Promise<PermissionEntity[]> => {
        return await PermissionEntity.find({relations: ['user', 'department']});
    }

    getById = async (id: string): Promise<PermissionEntity> => {
        return await PermissionEntity.findOne({id: parseInt(id)});
    }

    getByUserId = async (userid: string): Promise<PermissionEntity[]> => {
        const qb = await getRepository(PermissionEntity)
            .createQueryBuilder('permission')
            .leftJoinAndSelect('permission.user', 'user')
            .leftJoinAndSelect('permission.department', 'department')
            .where('permission.user.id = :userid', { userid });

        return qb.getMany();
    }

    create = async (permission: CreatePermissionDto): Promise<PermissionEntity> => {
        const newPerm = new PermissionEntity();
        newPerm.department.id = parseInt(permission.departmentID);
        newPerm.permission_type = permission.permission_type;
        newPerm.user.id = parseInt(permission.userID);
        return await newPerm.save();
    }

    update = async (permission: UpdatePermissionDto): Promise<PermissionEntity> => {
        const existingPermission = await this.getById(permission.id);
        if (existingPermission) {
            const updatedPermission = Object.assign(permission, existingPermission);
            return await updatedPermission.save();
        }
    }

    delete = async (id: string): Promise<DeleteResult> => {
       return await PermissionEntity.delete({id: parseInt(id)});
    }

}