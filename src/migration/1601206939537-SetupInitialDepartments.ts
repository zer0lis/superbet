import {MigrationInterface, QueryRunner} from "typeorm";
import {DepartmentEntity} from "../department/entity/department.entity";

export class SetupInitialDepartments1601206939537 implements MigrationInterface {

    private accountingDepartment: DepartmentEntity;
    private researchDepartment: DepartmentEntity;

    readonly addAccountingDepartmentQuery =
        `INSERT INTO "department" (name, status) VALUES( 'Accounting', 'activated');`

    readonly addResearchDepartmentQuery =
        `INSERT INTO "department" (name, status) VALUES( 'Research', 'activated');`

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(this.addAccountingDepartmentQuery);
        await queryRunner.query(this.addResearchDepartmentQuery);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await  queryRunner.query('DELETE FROM department');
    }

}
