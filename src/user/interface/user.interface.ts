import {DepartmentEntity} from "../../department/entity/department.entity";
import {PermissionEntity} from "../../permission/entity/permission.entity";
import {Status} from "../../shared/interface/status.type";

// user response object, without password
export interface UserRO {
    id: number;
    username: string;
    first_name: string;
    last_name: string;
    email: string;
    phone_number: string;
    departments: DepartmentEntity[] | null;
    permissions: PermissionEntity[];
    status: Status;
}

export interface UserLoginRO extends UserRO {
    token: any;
}