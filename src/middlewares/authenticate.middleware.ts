const jwt = require('express-jwt');
import {secret} from "../../config";

export const authenticateMiddleware = jwt({ secret, algorithms: ['HS256'] });

// export function authenticateMiddleware(req: Request, res: Response, next: NextFunction) {
//     console.log('auth')
//     // // Retrieve token from header
//     // const authorizationHeader = req.headers['authorization'];
//     // // Reserving variable for tokens
//     // let token;
//     // // Get received token
//     // if (authorizationHeader) token = authorizationHeader.split(' ')[1];
//     //
//     //
//     // // Token exists then validate to provide access or not
//     // if (token && !validator.isEmpty(token)) {
//     //     // Validate token with the secret
//     //     jwt.verify(token, secret, (err: any, decoded: any) => {
//     //         if (err) {
//     //             res.status(401).json({ error: 'Authentication refused! Unauthorized action.' });
//     //             res.end();
//     //         } else {
//     //             // Suggest
//     //         }
//     //     });
//     // } else {
//     //     res.status(401).json({ error: "Unauthorized! You must be logged in to use this service!" });
//     //     res.end();
//     // }
//     // next();
//     // return [
//     //     // authenticate JWT token and attach user to request object (req.user)
//     //
//     //
//     //     async (req: any, res: Response, next: NextFunction) => {
//     //         next();
//     //     }
//     // ];
// }