import {DepartmentEntity} from "../../department/entity/department.entity";
import {PermissionType} from "../interface/permission-type";


import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    ManyToOne,
    JoinTable, BeforeUpdate,
} from "typeorm";
import {UserEntity} from "../../user/entity/user.entity";

@Entity('permission')
export class PermissionEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => UserEntity, user => user.permissions, {eager: true, nullable: false})
    @JoinTable()
    user: UserEntity;

    @ManyToOne(() => DepartmentEntity, department => department.permissions, {eager: true, nullable: false})
    @JoinTable()
    department: DepartmentEntity;

    @Column({ type: "text" })
    permission_type: PermissionType;
}