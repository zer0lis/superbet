import {Controller} from "../shared/interface/controller.interface";
import {NextFunction, Request, Response, Router} from "express";
import {Service} from "./user.service";
import {DeleteResult} from "typeorm";
import { UserService } from "./user.service";
import {UserEntity} from "./entity/user.entity";
import {authorizeMiddleware} from "../middlewares/authorize.middleware";
import {authenticateMiddleware} from "../middlewares/authenticate.middleware";
import { authorizeIsAdminMiddleware } from "../middlewares/authorize-is-admin.middleware";

export class UserController implements Controller {
    public path = '/user';
    public router: Router = Router();
    userService: Service;

    constructor(private UserService: Service) {
        this.userService = UserService; // container.resolve(UserService);
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.post(`${this.path}/authenticate`, this.authenticate);     // public route
        this.router.get(`${this.path}`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.getAll);
        this.router.get(`${this.path}/:id`,   [authenticateMiddleware, authorizeMiddleware], this.getById);
        this.router.post(`${this.path}`,  [authenticateMiddleware, authorizeIsAdminMiddleware], this.create);
        this.router.put(`${this.path}/:id`,  [authenticateMiddleware, authorizeMiddleware], this.update);
        this.router.delete(`${this.path}/:id`,  [authenticateMiddleware, authorizeIsAdminMiddleware], this.delete);
    }

    authenticate = async (req: Request, res: Response, next: NextFunction) => {
        this.userService.authenticate(req.body)
            .then((user: any) => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
            .catch((err: Error) => next(err));
    }

    getAll = async (req: any, res: Response, next: NextFunction) => {
        this.userService.getAll()
            .then((users: UserEntity[]) => res.json(users))
            .catch((err: Error) => next(err));
    }

    getById = async (req: any, res: Response, next: NextFunction) => {
        const id = req.params.id;

        this.userService.getById(id)
            .then((user: UserEntity) => user ? res.json(user) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    create = async (req: any, res: Response, next: NextFunction) => {
        const newUser = req.body.user;
        if(!newUser) return res.status(401).json({ message: 'No user post data' });

        this.userService.create(newUser)
            .then((user: UserEntity) => user ? res.json(user) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    update = async (req: any, res: Response, next: NextFunction) => {
        const newUserData = req.body.user;

        if(!newUserData) return res.status(401).json({ message: 'No user post data' });

        this.userService.update(newUserData)
            .then((user: UserEntity) => user ? res.json(user) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    delete = async (req: any, res: Response, next: NextFunction) => {
        const id = req.params.id;

        this.userService.delete(id)
            .then((status: DeleteResult) => status ? res.json({message: 'UserEntity deleted'}) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

}

