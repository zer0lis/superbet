import {PermissionType} from "./permission-type";

export interface CreatePermissionDto {
    userID: string;
    departmentID: string;
    permission_type: PermissionType;
}