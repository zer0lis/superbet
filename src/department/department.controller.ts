import {NextFunction, Response, Router} from "express";
import {Service, UserService } from "../user/user.service";
import { DepartmentService } from "./department.service";
import {DeleteResult} from "typeorm";
import {Controller} from "../shared/interface/controller.interface";
import {DepartmentEntity} from "./entity/department.entity";
import {authenticateMiddleware} from "../middlewares/authenticate.middleware";
import {authorizeMiddleware} from "../middlewares/authorize.middleware";
import { authorizeIsAdminMiddleware } from "../middlewares/authorize-is-admin.middleware";


export class DepartmentController implements Controller {
    public path = '/department';
    public router: Router = Router();
    departmentService: Service;

    constructor(DepartmentService: Service) {
        this.departmentService = DepartmentService;
        this.initializeRoutes();

    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.getAll);
        this.router.get(`${this.path}/:id`,  [authenticateMiddleware, authorizeMiddleware], this.getById);
        this.router.post(`${this.path}`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.create);
        this.router.post(`${this.path}/:id/add/:userid`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.addUser);
        this.router.post(`${this.path}/:id/remove/:userid`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.removeUser);
        this.router.put(`${this.path}/:id`, [authenticateMiddleware, authorizeMiddleware], this.update);
        this.router.delete(`${this.path}/:id`, [authenticateMiddleware, authorizeIsAdminMiddleware], this.delete);
    }


    getAll = async (req: any, res: Response, next: NextFunction) => {
        this.departmentService.getAll()
            .then((departments: DepartmentEntity[]) => res.json(departments))
            .catch((err: Error) => next(err));
    }

    getById = async (req: any, res: Response, next: NextFunction) => {
        const id = req.params.id;

        this.departmentService.getById(id)
            .then((department: DepartmentEntity) => department ? res.json(department) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    create = async (req: any, res: Response, next: NextFunction) => {
        const newDepartment = req.body.department;

        if(!newDepartment) return res.status(401).json({ message: 'No user post data' });

        this.departmentService.create(newDepartment)
            .then((department: DepartmentEntity) => department ? res.json(department) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    update = async (req: any, res: Response, next: NextFunction) => {
        const newDepartmentData = req.body.department;

        if(!newDepartmentData) return res.status(401).json({ message: 'No department post data' });

        this.departmentService.update(newDepartmentData)
            .then((department: DepartmentEntity) => department ? res.json(department) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    addUser = async (req: any, res: Response, next: NextFunction) => {
        const departmentId = req.params.id;
        const userId = req.params.userid;

        this.departmentService.addUser(departmentId, userId)
            .then((department: DepartmentEntity) => department ? res.json(department) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

    removeUser = async (req: any, res: Response, next: NextFunction) => {
        const departmentId = req.params.id;
        const userId = req.params.userid;

        this.departmentService.removeUser(departmentId, userId)
            .then((department: DepartmentEntity) => department ? res.json(department) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }


    delete = async (req: any, res: Response, next: NextFunction) => {
        const id = req.params.id;

        this.departmentService.delete(id)
            .then((status: DeleteResult) => status ? res.json({message: 'UserEntity deleted'}) : res.sendStatus(404))
            .catch((err: Error) => next(err));
    }

}

