import {MigrationInterface, QueryRunner} from "typeorm";
import {UserEntity} from "../user/entity/user.entity";
import bcrypt from "bcrypt";
import {getRepository} from "typeorm";
import {DepartmentEntity} from "../department/entity/department.entity";

// todo refactor, either use plain queries or the typeorm sql builder

export class SetupInitialUsers1601207813175 implements MigrationInterface {

    private readonly accountingUser: UserEntity;
    private readonly researchUser: UserEntity;
    private readonly commonUser: UserEntity;

    // private readonly accountingUserPass = bcrypt.hashSync('accounting', 2);
    // private readonly researchUserPass = bcrypt.hashSync('research', 2);
    // private readonly commonUserPass = bcrypt.hashSync('common', 2);

    // private readonly addAccountingUserQuery =
    //     `INSERT INTO "user" (username,password) VALUES( 'accounting', '${this.accountingUserPass}');`
    // private readonly addResearchUserQuery =
    //     `INSERT INTO "user" (username,password) VALUES( 'research', '${this.researchUserPass}');`
    // private readonly addCommonUserQuery =
    //     `INSERT INTO "user" (username,password) VALUES( 'common', '${this.commonUserPass}');`



    public async up(queryRunner: QueryRunner): Promise<void> {
        // await queryRunner.query(this.addAccountingUserQuery);
        // await queryRunner.query(this.addResearchUserQuery);
        // await queryRunner.query(this.addCommonUserQuery);

        const userRepo = queryRunner.connection.getRepository(UserEntity);
        const departmentRepo = queryRunner.connection.getRepository(DepartmentEntity);
        //
        const accountingUser = new UserEntity();
        accountingUser.username = 'accounting_user';
        accountingUser.password = 'accounting';
        await accountingUser.save();

        const researchUser = new UserEntity();
        researchUser.username = 'research_user';
        researchUser.password = 'research';
        await researchUser.save();

        const commonUser = new UserEntity();
        commonUser.username = 'common_user';
        commonUser.password = 'common';
        await commonUser.save();
        //
        const accountingDepartment = await departmentRepo.findOne({name: "Accounting"}, {relations: ['users']});
        const researchDepartment = await departmentRepo.findOne({name: "Research"}, {relations: ['users']});


        accountingDepartment.users = [accountingUser, commonUser];
        await departmentRepo.save(accountingDepartment);

        researchDepartment.users = [researchUser, commonUser];
        await departmentRepo.save(researchDepartment);


    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('DELETE FROM user');
    }

}
