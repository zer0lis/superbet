import {MigrationInterface, QueryRunner, TableIndex, Table, TableColumn, TableForeignKey} from "typeorm";

// todo, to avoid starting the app to generate the schema

export class SetupSchema1601062879104 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: "user",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true
                },
                {
                    name: "username",
                    type: "varchar",
                },
                {
                    name: "password",
                    type: "varchar",
                },
                {
                    name: "email",
                    type: "varchar",
                    isNullable: true
                },
                {
                    name: "first_name",
                    type: "varchar",
                    isNullable: true
                },
                {
                    name: "last_name",
                    type: "varchar",
                    isNullable: true
                },
                {
                    name: "phone_number",
                    type: "varchar",
                    isNullable: true
                },
                {
                    name: "status",
                    type: "varchar",
                    default: "activated"
                }
            ]
        }), true);

        await queryRunner.createTable(new Table({
            name: "department",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true
                },
                {
                    name: "name",
                    type: "varchar",
                },
                {
                    name: "status",
                    type: "varchar",
                    default: "activated"
                }
            ]
        }), true)

        await queryRunner.createTable(new Table({
            name: "user_department",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true
                },
                {
                    name: "userID",
                    type: "int",
                },
                {
                    name: "departmentID",
                    type: "int",
                }
            ]
        }), true)

        await queryRunner.createTable(new Table({
            name: "permission",
            columns: [
                {
                    name: "id",
                    type: "int",
                    isPrimary: true
                },
                {
                    name: "departmentID",
                    type: "int",
                },
                {
                    name: "userID",
                    type: "int",
                },
                {
                    name: "permission_type",
                    type: "varchar",
                    default: "read"
                }
            ]
        }), true)

        await queryRunner.createForeignKey("user_department", new TableForeignKey({
            columnNames: ["userID"],
            referencedColumnNames: ["id"],
            referencedTableName: "user",
        }));

        await queryRunner.createForeignKey("user_department", new TableForeignKey({
            columnNames: ["departmentID"],
            referencedColumnNames: ["id"],
            referencedTableName: "department",
        }));

        await queryRunner.createForeignKey("permission", new TableForeignKey({
            columnNames: ["userID"],
            referencedColumnNames: ["id"],
            referencedTableName: "user",
        }));

        await queryRunner.createForeignKey("permission", new TableForeignKey({
            columnNames: ["departmentID"],
            referencedColumnNames: ["id"],
            referencedTableName: "department",
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const userTable = await queryRunner.getTable("user");
        const userForeignKey = userTable.foreignKeys.find(fk => fk.columnNames.indexOf("userID") !== -1);
        await queryRunner.dropForeignKey("question", userForeignKey);

        const departmentTable = await queryRunner.getTable("department");
        const departmentForeignKey = departmentTable.foreignKeys.find(fk => fk.columnNames.indexOf("departmentID") !== -1);
        await queryRunner.dropForeignKey("question", departmentForeignKey);

        const permissionTable = await queryRunner.getTable("department");
        // const permissionForeignKey = permissionTable.foreignKeys.find(fk => fk.columnNames.indexOf("departmentID") !== -1);
        // await queryRunner.dropForeignKey("question", permissionForeignKey);

        await queryRunner.dropTable("user_permission");
        await queryRunner.dropTable("user");
        await queryRunner.dropTable("department");
        await queryRunner.dropTable("permission");
    }
}
