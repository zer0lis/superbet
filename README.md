#### SuperBet Test Requirements:

1. Create CRUD APIs for managing user accounts.
- Users data: id, username, password, first name, last name, email, phone
number, department id, permissions, status*
2. Create CRUD APIs for managing departments.
- Department data: id, name, status*
3. Create endpoint for user authentication.
4. Create APIs for managing user permissions:
- Permission types:
- readonly - allows reading the data
- update - allows modifying the data
- Permission must be set at user account level
- Permission apply to user accounts and departments
- Permission must allow updating of own user data
- Permissions must allow user reading/updating users from specified departments
list.
- Permissions must allow user reading/updating departments from specified
departments list.

5. Create a seeder for initial user that has permissions to manage users and
departments.

You are free to use any system to store the data.
* status = active/inactive

######Mention based on specs
Permissions must allow user reading/updating users from specified departments list.
Permissions must allow user reading/updating departments from specified departments list.

So, a user can read/edit departments and if a department has users, the permissions apply to them also.

Typically, you can have a Resource/Permission intersect table table like: ``` user_with_permission, target_resource(can be users, departments, projects), and permission_type```
buty this doesnt seem to be the spec.
I designed it like ```user_with_permission, department, permission_type```
Based on the department, it can read/edit the department users or the department itself. There is no need do specify which user can read/update other user directly.
Thats what I understand from the specs.
Only the admin can use Permission API, create departments/users or add/remove users from departments.
I can do otherwise if specified. 
```shell
+-------------+--------------+----------------------------+
|                        user                             |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| ...                                                     |
+-------------+--------------+----------------------------+


+-------------+--------------+----------------------------+
|                        department                       |
+-------------+--------------+----------------------------+
| id          | int(11)      | PRIMARY KEY AUTO_INCREMENT |
| ...                                                     |
+-------------+--------------+----------------------------+

+---------------+--------------+--------------------------+
|              user_permissions                           |
+---------------+--------------+--------------------------+
| userId        | int(11)      | PRIMARY KEY FOREIGN KEY  |
| departmentId  | int(11)      | PRIMARY KEY FOREIGN KEY  |
| permissionType| enum("read") | PRIMARY KEY FOREIGN KEY  |
+-------------+--------------+----------------------------+
```


### About the solution
It used Typescript, express and Typeorm. The persistance system is SQLite. It uses JWT for auth
Besides this, I use minimum libraries: jsonwebtoken, class-validator, bcrypt, helmet, cors.
It uses request validation. It uses a user flag to denote the admin(the easiest solution).


The project is structured between DDD and MVC, each domain contains a typical MVC structure
It uses Typescript dependency injection without resorting to libraries like tsyringe or nest

Folder structure:

```src``` => project source files

```src/config``` => app configuration (PORT and JWT secret)

```src/department``` => department domain

```src/user``` => user domain

```src/permission``` => permission domain

```src/middlewares``` => http middlewares: authentication, authorization(main requirement), error handler

```src/migration``` => db migrations or seeds

```src/shared``` => reusable code by each domain code

A domain contains(for example user domain):
```entity(the data model with no coupling), interfaces(domain types or DTOs), controller, service```


#### Project setup
Ensure you have LTS node and npm and then run
```npm i```

#### Project scripts
```npm start``` starts the project but the database is empty(schema is created)

```npm prebuild``` prebuild, like checking for linting errors

```npm build``` prebuild, like checking for linting errors

```npm run migration:run``` run migrations

```npm run migration:revert``` rollback migrations

###### Run with docker
```docker build -t api-server . ```

```docker run -t -i -p 3000:3000 api-server```
###### Run with docker-compose
```docker-compose up```



##### Ready made users and departments
- departments: ```{id: 1, name: accounting}, {id: 1, name: research}```
- users: 
- ```{id:1, username: admin, password: admin}```,
- ```{id:1, username: common, password: common_user, departments: [{id:1, name: accounting}, {id:2, name: research}]}```,
- ```{id:1, username: research, password: research_user, departments: [{id:2, name: research}]}```,
- ```{id:1, username: accounting, password: accounting_user, departments: [{id:1, name: accounting}]}```

#### Running API tests locally

To locally run the provided Postman collection against your backend, execute:

```
APIURL=http://localhost:3000/api ./run-api-tests.sh
```

For more details, see [`run-api-tests.sh`](run-api-tests.sh).

#### API Specs - You can find included a Postman collection to import
##### User API /user/
Authenticate into the system - POST /user/authenticate

Request body example: ```{"user": {"username": "test", "pass": "password"}}```

All users - GET /user/

Get a user - GET /user/:id

Create user - POST /user/

Request body example:  ```{"user": {"username": "test", "pass": "password"}}```

Update an user - PUT /user/:id

Request body example:  ```{"user": {"username": "test", "email": "email@e.ro"}}```

Delete an user - DELETE /user/:id

##### Department API /department/

All departments - GET /department/

Get a department - GET /department/:id

Create department - POST /department/

Request body example:  ```{"department": {"name": "Support department"}}```

Update an department - PUT /deparment/:id

Request body example:  ```{"department": {"name": "Billing"}}```

Add a user to a department - POST /department/:id/add/:userid

Remove a user from a department - POST /department/:id/remove/:userid

Delete an department - DELETE /user/:id


##### Permission API /permission/

All permissions - GET /permission/

Get a permission - GET /permission/:id

Create permission - POST /permission/

Request body example:  ```{"permission": {"userID": 1, "departmentID": 1, "permission_type": "update"}}```

Update a permission - PUT /permission/:id

Request body example:  ```{"permission": {"userID": 1, "departmentID": 1, "permission_type": "read"}}```

Delete an permission - DELETE /permission/:id