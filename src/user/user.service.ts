import {secret} from '../../config';
import {DeleteResult, getRepository} from "typeorm";
import bcrypt from "bcrypt";
import {UserLoginRO} from "./interface/user.interface";
import {UserEntity} from "./entity/user.entity";
import {validate} from "class-validator";
import {ILogin} from "./interface/login.interface";
import {CreateUserDto} from "./interface/dto/create-user.dto";
import {UpdateUserDto} from "./interface/dto/update-user.dto";
const jwt = require('jsonwebtoken');

// todo refactor
export interface Service {
    getAll(): Promise<any[]>
    getById(id: string): Promise<any>
    create(entity: any, relationId?: number): Promise<any>
    update(entity: any, relationId?: number): Promise<any>
    delete(id: string): Promise<DeleteResult>;

    authenticate?(body: ILogin): Promise<UserLoginRO>;
    getUserPermissions?(body: ILogin): Promise<UserLoginRO>;
    getByUserId?(id: string): Promise<any>;

    addUser?(id: string, userid: string): Promise<any>
    removeUser?(id: string, userid: string): Promise<any>
}

export class UserService implements Service {

    authenticate = async ({username, pass}: ILogin): Promise<any> => {
        const user = await getRepository(UserEntity)
            .createQueryBuilder("user")
            .leftJoinAndSelect("user.departments", "departments")
            .leftJoinAndSelect("user.permissions", "permissions")
            .select(['user', 'departments', 'permissions'])
            .addSelect('user.password')
            .where({username: username})
            .getOne();

        if (!user) {
            throw new Error("Invalid username");
        }
        const isCorrectPassword = await bcrypt.compare(pass, user.password);
        if (!isCorrectPassword) {
            throw new Error("Invalid password");
        }

        const token = jwt.sign({id: user.id, username, departments: user.departments, status: user.status, role: user.role}, secret);
        const {password, ...userWithoutPassword} = user;
        return {
            ...userWithoutPassword,
            token
        };
    }

    getAll = async (): Promise<UserEntity[]> => {
        return await UserEntity.find({relations: ['departments', 'permissions']})
    }

    getById = async (id: string): Promise<UserEntity> => {
        return await UserEntity.findOneOrFail({id: parseInt(id)}, {relations: ['departments', 'permissions']})
    }

    create = async (user: CreateUserDto): Promise<UserEntity> => {
        // check uniqueness of username/email
        const {email, username, password} = user;
        const qb = await getRepository(UserEntity)
            .createQueryBuilder('user')
            .where('user.email = :email', { email })
            .orWhere('user.username = :username', { username });

        const existingUser = await qb.getOne();
        if (existingUser) {
            throw new Error('REGISTRATION.USER_ALREADY_REGISTERED');
        }

        const newUser = new UserEntity();
        // newUser = Object.assign(newUser, user);
        newUser.username = username;
        newUser.password = password;

        const errors = await validate(newUser);
        if (errors.length > 0) {
            throw new Error('REGISTRATION.MISSING_MANDATORY_PARAMETERS');
        } else {

            return await newUser.save();
        }
    }

    update = async (user: UpdateUserDto): Promise<UserEntity> => {
        const existingUser = await this.getById(user.id);
        if (!existingUser) {
            throw new Error('REGISTRATION.NO_USER_TO_UPDATE');
        }
        const updatedUser = Object.assign(existingUser, user);
        const errors = await validate(updatedUser);
        if (errors.length > 0) {
            throw new Error(`REGISTRATION.MISSING_MANDATORY_PARAMETERS`);
        } else {
            return await updatedUser.save();
        }
    }

    delete = async (id: string): Promise<DeleteResult> => {
       return UserEntity.delete({id: parseInt(id)})
    }

}