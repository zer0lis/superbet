export interface UpdateDepartmentDto {
    id: string;
    name: string;
}