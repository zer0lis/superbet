import {
    BaseEntity,
    BeforeUpdate,
    Column,
    Entity,
    JoinTable,
    ManyToMany,
    OneToMany,
    PrimaryGeneratedColumn
} from "typeorm";
import {UserEntity} from "../../user/entity/user.entity";
import {PermissionEntity} from "../../permission/entity/permission.entity";
import {Status} from "../../shared/interface/status.type";

@Entity('department')
export class DepartmentEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "text" })
    name: string;

    @ManyToMany(type => UserEntity, user => user.departments)
    @JoinTable({name: 'user_department'})
    users: UserEntity[];

    @OneToMany(type => PermissionEntity, permission => permission.department, {nullable: true})
    permissions: PermissionEntity[];

    @Column({type: "text", default: "activated"})
    status: Status;
}