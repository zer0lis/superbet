import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    BeforeInsert,
    JoinTable,
    ManyToMany, OneToMany, BeforeUpdate
} from "typeorm";
import bcrypt from 'bcrypt';
import {DepartmentEntity} from "../../department/entity/department.entity";
import {IsEmail, IsNotEmpty, IsOptional, IsPhoneNumber, IsString} from "class-validator";
import {PermissionEntity} from "../../permission/entity/permission.entity";
import {Status} from "../../shared/interface/status.type";

export type Role =  "admin" | "user";

@Entity('user')
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "text" })
    @IsString()
    @IsNotEmpty()
    username: string;

    @Column({ type: "text" , select: false})
    @IsString()
    @IsNotEmpty()
    password: string;

    @Column({ type: "text", nullable: true })
    @IsEmail()
    @IsOptional()
    email: string;

    @Column({ type: "text", nullable: true })
    @IsString()
    @IsOptional()
    first_name: string;

    @Column({ type: "text", nullable: true })
    @IsString()
    @IsOptional()
    last_name: string;

    @Column({ type: "text", nullable: true })
    @IsPhoneNumber('Romania')
    @IsOptional()
    phone_number: string;

    @ManyToMany(type => DepartmentEntity, department => department.users, {eager: true})
    @JoinTable({name: 'user_department'})
    departments: DepartmentEntity[];

    @OneToMany(type => PermissionEntity, permission => permission.user, {nullable: true})
    permissions: PermissionEntity[];

    @Column({ type: "text", default: "activated" })
    status: Status;

    @Column({ type: "text", default: "user" })
    role: Role;

    @BeforeInsert()
    hashPassword() {
        this.password = bcrypt.hashSync(this.password, 2);
    }
}


