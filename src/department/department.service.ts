import {DeleteResult} from "typeorm";
import {Service} from "../user/user.service";
import {DepartmentEntity} from "./entity/department.entity";
import {UserEntity} from "../user/entity/user.entity";
import {CreateDepartmentDto} from "./interface/create-department.dto";
import {UpdateDepartmentDto} from "./interface/update-department.dto";

export class DepartmentService implements Service {

    private userService: Service;
    constructor(private UserService: Service) {
        this.userService = UserService;
    }

    getAll = async (): Promise<DepartmentEntity[]> => {
        return await DepartmentEntity.find({relations: ["users"]})
    }

    getById = async (id: string): Promise<DepartmentEntity> => {
        return await DepartmentEntity.findOneOrFail({id: parseInt(id)}, {relations: ["users"]})
    }

    // async getDepartmentsByUserId(id: string): Promise<DepartmentEntity[]> {
    //     return await DepartmentEntity.find({user.id: id});
    // }

    areUsersFromTheSameDepartment = async (userId1: string, userId2: string): Promise<boolean> => {
        const user1DepartmentsIds = (await this.userService.getById(userId1)).departments.map( (d: DepartmentEntity) => d.id);
        const user2DepartmentsIds = (await this.userService.getById(userId2)).departments.map( (d: DepartmentEntity) => d.id);

        return user1DepartmentsIds.some ( (d: number) => user2DepartmentsIds.includes(d));
    }

    create = async (department: CreateDepartmentDto): Promise<DepartmentEntity> => {
        const newDepartment = Object.assign(department, new DepartmentEntity());
        return newDepartment.save();
    }

    update = async (department: UpdateDepartmentDto): Promise<DepartmentEntity> => {
        const existingDepartment = await this.getById(department.id)
        const newUser = Object.assign(department, existingDepartment);
        return newUser.save();
    }

    addUser = async (id: string, userId: string) => {
        const user = await UserEntity.findOne(userId, {relations: ['departments']});
        if (!user) throw Error('ADD_DEPARTMENT_USER.NO_USER');

        const department = await this.getById(id);
        if (!department) throw Error('ADD_DEPARTMENT_USER.NO_DEPARTMENT');

        if (department.users && department.users.length) {
            // check if user is already present in the department
            const isUserAlreadyPresent = department.users.filter((u: UserEntity) => u.id === parseInt(userId));
            if (isUserAlreadyPresent) return department; // or throw error

            department.users.push(user)
        } else {
            department.users = [user]
        }

        return await DepartmentEntity.save(department)
    }

    removeUser = async (id: string, userId: string) => {
        const user = await UserEntity.findOne(userId, {relations: ['departments']});
        if (!user) throw Error('ADD_DEPARTMENT_USER.NO_USER');

        const department = await this.getById(id);
        if (!department) throw Error('ADD_DEPARTMENT_USER.NO_DEPARTMENT');

        if (department.users && department.users.length) {
            department.users = department.users.filter( (u: UserEntity) => u.id !== parseInt(userId));

            return await DepartmentEntity.save(department)
        }
    }

    delete = async (id: string): Promise<DeleteResult> => {
        return DepartmentEntity.delete({id: parseInt(id)})
    }

}