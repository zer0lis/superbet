import {Request, Response, NextFunction} from "express";
import {PermissionService} from "../permission/permission.service";
import {Service, UserService} from "../user/user.service";
import {DepartmentService} from "../department/department.service";
import {PermissionEntity} from "../permission/entity/permission.entity";


export const authorizeMiddleware = async (req: any, res: Response, next: NextFunction) => {

    if (req.user.role === 'admin') {
        next();
    }

    if (req.method === "POST" || req.method === "DELETE") { // only admins can add departments, users or delete anything
        return res.status(401).json({message: 'Unauthorized to create or delete anything'});
    }

    const userService: Service = new UserService();
    const permissionService: Service = new PermissionService();
    const departmentService = new DepartmentService(userService);

    const permissions = await permissionService.getByUserId(req.user.id);

    let allowed = false;

    if (req.path.includes('department')) {
        const departmentId = parseInt(req.params.id);

        if (req.method === "GET") {
            if(!departmentId) return res.status(401).json({message: 'Only the admin can request the department list'});
            allowed = permissions.some((p: PermissionEntity) => p.department.id === departmentId)
        }
        if (req.method === "PUT") {
            allowed = permissions.some((p: PermissionEntity) => p.department.id === departmentId && p.permission_type === "update")
        }

    }

    if (req.path.includes('user')) {
        // check to see if the target user is within an owned department
        const targetUserId = req.params.id;
        if(!targetUserId) return res.status(401).json({message: 'Only the admin can request the user list'});

        const sameDepartment = await departmentService.areUsersFromTheSameDepartment(req.user.id, targetUserId);
        if (!sameDepartment) {
            return res.status(401).json({message: 'Unauthorized, the requested user is not in your department'});
        }

        if (req.method === "GET") {
            allowed = permissions.some((p: PermissionEntity) => p.permission_type === "read")
        }

        if (req.method === "PUT") {
            allowed = permissions.some((p: PermissionEntity) => p.permission_type === "update")

        }
    }

    if (!allowed) {
        return res.status(401).json({message: 'Unauthorized, you lack permissions to perform this action'});
    }

    next();
}