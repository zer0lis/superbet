import {PermissionType} from "./permission-type";

export interface UpdatePermissionDto {
    id: string;
    userID: string;
    departmentID: string;
    permission_type: PermissionType;
}