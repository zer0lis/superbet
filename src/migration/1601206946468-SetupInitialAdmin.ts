import {MigrationInterface, QueryRunner} from "typeorm";
import bcrypt from 'bcrypt';

export class SetupInitialAdmin1601206946468 implements MigrationInterface {
    name = 'AddAdminUser1601062879104'
    private hashedPass = bcrypt.hashSync('admin', 2);

    readonly addAdminUserQuery =
        `INSERT INTO "user" (username,password, role) VALUES( 'admin', '${this.hashedPass}', 'admin');`

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(this.addAdminUserQuery);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await  queryRunner.query('DELETE FROM user')
    }
}
