import App from './app';
// todo
// import 'dotenv/config';
// import {validateEnv} from './_helpers/utils';
// validateEnv();
import { createConnection } from "typeorm";
import {UserController} from "./src/user/user.controller";
import {Service, UserService} from "./src/user/user.service";
import {DepartmentController} from "./src/department/department.controller";
import {PermissionController} from "./src/permission/permission.controller";
import {DepartmentService} from "./src/department/department.service";
import {PermissionService} from "./src/permission/permission.service";

( async () => {

    const userService = new UserService(); //container.resolve(UserController);
    const departmentService: Service = new DepartmentService(userService);
    const permissionService: Service = new PermissionService();

    const connection = await createConnection({
        type: "sqlite",
        database: "temp/sqlitedb.db",
        synchronize: true,
        logging: false,
        entities: ["src/*/entity/*.ts"],
        migrations: ["src/migration/**/*.ts"],
        subscribers: ["src/subscriber/**/*.ts"]
    });

    const app = new App(
        [
            new UserController(userService),
            new DepartmentController(departmentService),
            new PermissionController(permissionService)
        ],
    );

    app.listen();

})();