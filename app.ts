import * as bodyParser from 'body-parser';
const express = require('express')
import * as helmet from "helmet";
import {errorHandlerMiddleware} from "./src/middlewares/error-handler.middleware";
import {port} from "./config";
import {Controller} from "./src/shared/interface/controller.interface";



export class App {
    public app: any;

    constructor(controllers: Controller[]) {
        this.app = express();
        this.initializeMiddlewares();
        this.initializeControllers(controllers);
        this.initializeErrorHandling();
        this.initializeExtraSecurity();
    }

    public listen() {
        this.app.listen(port, () => {
            console.log(`App listening on the port ${port}`);
        });
    }

    public getServer() {
        return this.app;
    }

    private initializeMiddlewares() {
        this.app.use(bodyParser.json());
    }

    private initializeErrorHandling() {
        this.app.use(errorHandlerMiddleware);
    }

    private initializeExtraSecurity() {
        //this.app.use(helmet());
    }

    private initializeControllers(controllers: Controller[]) {
        controllers.forEach((controller) => {
            this.app.use('/', controller.router);
        });
    }
}

export default App;