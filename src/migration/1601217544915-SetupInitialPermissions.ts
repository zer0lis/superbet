import {MigrationInterface, QueryRunner} from "typeorm";
import {UserEntity} from "../user/entity/user.entity";
import {DepartmentEntity} from "../department/entity/department.entity";
import {PermissionEntity} from "../permission/entity/permission.entity";

export class SetupInitialPermissions1601217544915 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const userRepo = queryRunner.connection.getRepository(UserEntity);
        const departmentRepo = queryRunner.connection.getRepository(DepartmentEntity);
        const permissionRepo = queryRunner.connection.getRepository(PermissionEntity);

        const accountingDepartment = await departmentRepo.findOne({name: "Accounting"});
        const researchDepartment = await departmentRepo.findOne({name: "Research"});

        const accountingUser = await userRepo.findOne({username: "accounting_user"});
        const researchUser = await userRepo.findOne({username: "research_user"});
        const commonUser = await userRepo.findOne({username: "common_user"});

        // common user has read perm to its groups(both initial groups)
        // accounting and research users have update permission on their group

        const commonUserPermissionForAccounting = new PermissionEntity();
        commonUserPermissionForAccounting.department = accountingDepartment;
        commonUserPermissionForAccounting.user = commonUser;
        commonUserPermissionForAccounting.permission_type = "read";
        await permissionRepo.save(commonUserPermissionForAccounting);

        const commonUserPermissionForResearch = new PermissionEntity();
        commonUserPermissionForResearch.department = researchDepartment;
        commonUserPermissionForResearch.user = commonUser;
        commonUserPermissionForResearch.permission_type = "read";
        await permissionRepo.save(commonUserPermissionForResearch);

        const accountingUserPermissionForAccounting = new PermissionEntity();
        accountingUserPermissionForAccounting.department = accountingDepartment;
        accountingUserPermissionForAccounting.user = accountingUser;
        accountingUserPermissionForAccounting.permission_type = "update";
        await permissionRepo.save(accountingUserPermissionForAccounting);

        const researchUserPermissionForResearch = new PermissionEntity();
        researchUserPermissionForResearch.department = researchDepartment;
        researchUserPermissionForResearch.user = researchUser;
        researchUserPermissionForResearch.permission_type = "update";
        await permissionRepo.save(researchUserPermissionForResearch);

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('DELETE FROM permission');
    }

}
