import {Request, Response, NextFunction} from "express";

export const authorizeIsAdminMiddleware = async (req: any, res: Response, next: NextFunction) => {
    if (req.user.role === 'admin') {
        next();
    } else {
        return res.status(401).json({message: 'Unauthorized, admin role required'});
    }
}